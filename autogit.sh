#!/bin/bash
#Nabilus // Autogit V1
#Script qui creer automatiquement le dossier local et le nombre souhaiter de repos et branches

echo "Entrez le nom du dossier pour le nouveau projet :"
read nomDossier

echo "Entrez votre nom :"
read nomContributeur

echo "Entrez le nom du projet :"
read nomProjet

echo "Entrez les principales technologies utilisées :"
read technologies

echo "Entrez le nom de votre compte Gitlab :"
read nomCompte

echo "Entrez le nombre de dépôts distants que vous souhaitez créer :"
read nombreDepots

echo "Création du dossier $nomDossier..."
mkdir "$nomDossier" && cd "$nomDossier" || exit

git init

echo -e "# $nomProjet\n\nContributeur principal : $nomContributeur\n\nPrincipales technologies utilisées : $technologies" > README.md

depots=()

echo "Entrez le nom de la branche principale (ex : main) :"
read branchePrincipale
git checkout -b "$branchePrincipale"

git add .
git commit -m "Premier commit : initialisation du projet"

for ((i = 1; i <= nombreDepots; i++)); do
    echo "Entrez le nom du dépôt distant $i :"
    read nomDepot
    depotURL="https://gitlab.com/$nomCompte/$nomDepot.git"
    git remote add "$nomDepot" "$depotURL"
    depots+=("$nomDepot")
    git push --set-upstream "$nomDepot" "$branchePrincipale"
done

count=1

while true; do
    count=$((count+1))
    read -p "Entrez le nom de la branche $count à créer (ou appuyez sur Enter pour terminer) : " nouvelleBranche
    
    if [ -z "$nouvelleBranche" ]; then
        break
    fi
    
    git checkout -b "$nouvelleBranche"
    echo -e "\n\n## $nouvelleBranche branch\n\nContributeur principal : $nomContributeur\n\nPrincipales technologies utilisées : $technologies" >> README.md
    git add README.md
    git commit -m "Ajout des informations pour $nouvelleBranche"
    
    for depot in "${depots[@]}"; do
        git push --set-upstream "$depot" "$nouvelleBranche"
    done
    
    echo "La branche $nouvelleBranche a été créée en tant que branche $count."
done

echo "Le projet a été configuré avec succès sur Gitlab, mon ami."
